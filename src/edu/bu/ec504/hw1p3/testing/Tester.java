package edu.bu.ec504.hw1p3.testing;

import edu.bu.ec504.hw1p3.compressors.Compressor;
import edu.bu.ec504.hw1p3.util.CompressedText;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class Tester {

    // MAIN method

    // METHODS
    /**
     * Performs a test of encoding and decoding of a given plaintext file.
     * @param plainFile The name of the file whose contents should be tested.
     * @return a {@link TestResult} object with statistics about the test.
     *
     * @throws IOException If one of the files (plain, compressed, or decompressed) cannot be opened appropriately.
     * @throws ClassNotFoundException for a deserialization error.
     */
    private static TestResult testFile(Compressor theCompressor, String plainFile) throws IOException, ClassNotFoundException {
        // constants
        final String compressedFile = "compressed.txt";    // the file where the compression of the URL data is put
        final String decompressedFile = "decompressed.txt";// the file where the decoding of the encodedFile is put

        // variables
        long initTime, endTime;        // used to time the encoding process
        long origSize, compressedSize; // used to compare the uncompressed and compressed file sizes
        File theFile;                  // used for File queries

        // 0. Record the file size
        theFile = new File(plainFile);
        origSize = theFile.length();

        // 1. (Re-)read [from the file] and compress it
        BufferedReader in = new BufferedReader( new InputStreamReader(new FileInputStream(plainFile)));
        initTime = System.currentTimeMillis();
        CompressedText compressed = theCompressor.processPlainText(in);
        endTime = System.currentTimeMillis();

        // 2. Serialize the compressed object into a file
        ObjectOutputStream outStream = new ObjectOutputStream(new FileOutputStream(compressedFile));
        outStream.writeObject(compressed);
        outStream.close();
        // ... get the size of the file
        theFile = new File(compressedFile);
        compressedSize = theFile.length();

        // 3. Read the encoded object from the file and uncompress it (should provide the same as the data from the input URL)
        ObjectInputStream inStream = new ObjectInputStream(new FileInputStream(compressedFile));
        CompressedText uncompressed = (CompressedText) inStream.readObject();
        inStream.close();
        PrintStream outDecode = new PrintStream(new FileOutputStream(decompressedFile));
        outDecode.println(theCompressor.uncompress(uncompressed));
        outDecode.close();

        // 4. Record statistics about the test
        boolean identicalQ = identical(plainFile, decompressedFile);
        if (!identicalQ) {
            throw new RuntimeException("original doesn't match uncompressed text: "+
                    plainFile.substring(0,10)+"... vs. "+
                    decompressedFile.substring(0,10));
        }
        return new TestResult(endTime - initTime, origSize, compressedSize, identicalQ);
    }

    /**
     * Performs a test of encoding and decoding a given string.
     * @param plaintext The string to test
     * @return a {@link TestResult} object with statistics about the test.
     */
    public static TestResult testString(Compressor theCompressor, String plaintext)
        throws IOException, ClassNotFoundException {
        final String plainFile = "plain.txt";              // tthe plain text file that will contain the string

        // based on https://www.baeldung.com/java-write-to-file
        BufferedWriter writer = new BufferedWriter( new FileWriter(plainFile));
        writer.write(plaintext);
        writer.close();

        return testFile(theCompressor, plainFile);
    }


    /**
     * Performs a test of encoding and decoding a given URL
     * @param theCompressor The compressor object to test
     * @param inURL The URL to test - must be http only (no https handshaking)
     * @return a {@link TestResult} object with statistics about the test.
     * @throws IOException If one of the files (plain, compressed, or decompressed) cannot be opened appropriately.
     * @throws ClassNotFoundException for a deserialization error.
     */
    public static TestResult testUrl(Compressor theCompressor, String inURL) throws IOException, ClassNotFoundException {
        final String plainFile = "plain.txt";              // the plain text file that will contain the URL input

        // Read the URL into a file
        // ... https://www.baeldung.com/java-download-file
        URL uu = new URL(inURL);
        ReadableByteChannel rbc = Channels.newChannel(uu.openStream());
        FileOutputStream fos = new FileOutputStream(plainFile);
        fos.getChannel().transferFrom(rbc,0,Long.MAX_VALUE);

        // Continue the test on the file
        return testFile(theCompressor, plainFile);
    }

    /**
     * Checks whether <code>file1</code> and <code>file2</code> are identical without consideration of termination characters ('\n','\r')
     * @param file1 one of the files to compare
     * @param file2 one of the files to compare
     * @return true iff the contents of file1 and file2 are identical modulo termination characters.
     */
    private static boolean identical(String file1, String file2) {
        // ... also based on http://www.exampledepot.com/egs/java.io/CopyFile.html
        String sf1, sf2;
        try {
            sf1 = cleanString(readFile(file1));
            sf2 = cleanString(readFile(file2));
        }
        catch (IOException e) {return false;} // any exceptions are treated as a lack of a match

        return sf1.compareTo(sf2)==0;

    }

    /**
     * Removes all non-printable characters from a string.
     * Based on:  https://howtodoinjava.com/regex/java-clean-ascii-text-non-printable-chars/
     * @param str
     */
    private static String cleanString(String str) {
        str=str.replaceAll("[^\\x00-\\x7F]", ""); // non-ASCII
        str = str.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", ""); // ASCII control characters - see http://www.unicode.org/reports/tr18/#Compatibility_Properties
        str = str.replaceAll("\\p{C}", ""); // non-printable characters - see http://www.unicode.org/reports/tr18/#General_Category_Property
        return str;
    }

    /**
     * Reads a file into a string
     * @param file1 the file to read
     * @return The string representing the file
     * @throws IOException If file reading fails.
     */
    private static String readFile(String file1) throws IOException {
        StringBuilder data = new StringBuilder(1024);
        BufferedReader rd = new BufferedReader(new FileReader(file1));
        char[] buf = new char[1024];

        int len;
        while ((len = rd.read(buf)) != -1)
            data.append(buf, 0, len);

        rd.close();
        return data.toString();
    }

    // NESTED CLASSES

}
