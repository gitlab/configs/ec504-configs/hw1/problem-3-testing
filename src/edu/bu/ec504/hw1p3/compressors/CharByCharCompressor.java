package edu.bu.ec504.hw1p3.compressors;

import edu.bu.ec504.hw1p3.util.Atom;
import edu.bu.ec504.hw1p3.util.CompressedText;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * A simple implementation of the {@link Compressor} class.
 * This implementation ignores the dictionary entirely, and simply encodes each character of
 * the input as a trivial {@link Atom}, indexed against the empty string.
 */
public class CharByCharCompressor extends Compressor {

  /**
   * @inheritDoc
   * @return
   */
  @Override
  public CompressedText processPlainText(BufferedReader in) {
    final int EOF = -1; // end of file delimiter
    ArrayList<Atom> result = new ArrayList<>();

    try {
      for (int nextCharOrd = in.read(); // ordinal value of the next character in the input
          nextCharOrd != EOF;
          nextCharOrd=in.read()) {
        char nextChar = (char) nextCharOrd;
        result.add(
            new Atom(
                0,                 // refers to the empty string "", the 0-th item in the dictionary
                String.valueOf(nextChar)    // encapsulates the character nextChar as a String
            )
        );
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return new CompressedText(result);
  }
}
