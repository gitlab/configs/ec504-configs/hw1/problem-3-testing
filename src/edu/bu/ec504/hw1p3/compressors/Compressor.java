package edu.bu.ec504.hw1p3.compressors;

import edu.bu.ec504.hw1p3.util.Atom;
import edu.bu.ec504.hw1p3.util.CompressedText;

import java.io.BufferedReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public abstract class Compressor implements Serializable {

    /**
     * DEBUG==true prints debugging information during operation.
     * DEBUG==false does not print this debugging information
     */
    public static boolean DEBUG = false;


    // METHODS

    // CONSTRUCTORS
    Compressor() { }

    /**
     * Process additional plaintext from <code>in</code> and incorporates it into this compressed object.
     *
     * @param in A BufferedReader linked to an input source.
     */
    public abstract CompressedText processPlainText(BufferedReader in);

    /**
     * Decodes the meta-data stored within this object into an uncompressed text.
     * Reads the file, Atom by Atom, reconstructing the dictionary that was used
     * to produce it *and* the string that produced the dictionary.
     *
     * @return The uncompressed text corresponding to this object.
     */
    final public String uncompress(CompressedText theCompressedText) {
        final ArrayList<String> dict = emptyDict;  // a dictionary to be used in compression.

        StringBuilder result = new StringBuilder();
        for (Atom anAtom : theCompressedText.getList()) {
            String nextStr = dict.get(anAtom.getIndex()) + anAtom.getSuffix(); // the next String decoded from the theCompressedText
            result.append(nextStr);
            dict.add(nextStr); // add this string to the dictionary

            if (DEBUG) {
                System.out.println("Decoding Atom "+anAtom+ " to "+nextStr);
            }
        }
        return result.toString();
    }

    // FIELDS
    private static final long serialVersionUID = 1L; // to allow serialization
    protected static final ArrayList<String> emptyDict = new ArrayList<>(Collections.singletonList("")); // contains just one string, the empty string

}
