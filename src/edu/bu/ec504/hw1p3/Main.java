package edu.bu.ec504.hw1p3;

import edu.bu.ec504.hw1p3.compressors.CharByCharCompressor;
import edu.bu.ec504.hw1p3.compressors.LineByLineCompressor;
import edu.bu.ec504.hw1p3.compressors.MyCompressor;
import edu.bu.ec504.hw1p3.testing.Tester;
import java.io.IOException;

public class Main {
  // CONSTANTS
  private final static String URLExample = "https://en.wikipedia.org/wiki/Data_compression_ratio"; // the default URL to test
  private final static String StringExample = "can you can a can as a canner can can a can?";
   private final static String SimpleExample = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

  public static void main(String[] args) {
    try {
      System.out.println("Trying simple example: "+SimpleExample);
      System.out.println(Tester.testString(new MyCompressor(), SimpleExample));
      System.out.println("... passed"); // if we got here, we have passed the test

      System.out.println("Trying standard example: "+StringExample);
      System.out.println(Tester.testString(new MyCompressor(), StringExample));
      System.out.println("... passed"); // if we got here, we have passed the test

      System.out.println("Trying URL example: "+URLExample);
      System.out.println(Tester.testUrl(new MyCompressor(), URLExample));
      System.out.println("... passed"); // if we got here, we have passed the test

    } catch (Exception e) {
      throw new RuntimeException("Test failed: "+e);
    }
  }
}
